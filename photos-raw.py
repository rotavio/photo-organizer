#!/usr/bin/env python
import exifread
import datetime
import hashlib
import os
import re
import shutil
import subprocess

# config no trailing slashes please
base_path = '/Volumes/Arquivos'
# source_path = base_path + '/jpg/2019'

# base_path = './sample'
source_path = base_path + '/photos-input'

output_path = base_path + '/photos-analisadas/raw'

# All dates
match_path = output_path + '/match'

mismatch_all_path = output_path + '/mismatch_all'
mismatch_original_path = output_path + '/mismatch_original'
mismatch_digitized_path = output_path + '/mismatch_digitized'
mismatch_name_path = output_path + '/mismatch_name'

# No dates
none_path = output_path + '/none'

# Two Dates
no_original_match_path = output_path + '/no_original_match'
no_digitized_match_path = output_path + '/no_digitized_match'
no_name_match_path = output_path + '/no_name_match'
no_original_mismatch_path = output_path + '/no_original_mismatch'
no_digitized_mismatch_path = output_path + '/no_digitized_mismatch'
no_name_mismatch_path = output_path + '/no_name_mismatch'

# One Date
only_filename_path = output_path + '/only_filename'
only_original_path = output_path + '/only_original'
only_digitized_path = output_path + '/only_digitized'


# check if destination path is existing create if not
# if not os.path.exists(destin_path):
#     os.makedirs(destin_path)

# file hash function


def hash_file(filename):

    # make a hash object
    h = hashlib.sha1()

    # open file for reading in binary mode
    with open(filename, 'rb') as file:

        # loop till the end of the file
        chunk = 0
        while chunk != b'':
            # read only 1024 bytes at a time
            chunk = file.read(1024)
            h.update(chunk)

    # return the hex representation of digest
    return h.hexdigest()


def generate_folder_name(date_info):
    date_str = "{1}{0}{1}-{2}{0}{1}-{2}-{3}".format(os.sep,
                                                    date_info['year'], date_info['month'], date_info['day'])
    return date_str


def move_file_not_in_use(filename, output_folder, date_folder=None):

    if date_folder:
        output_folder = os.path.join(
            output_folder, generate_folder_name(date_folder))

    # check if destination path is existing create if not
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    basename = os.path.basename(filename)
    head, tail = os.path.splitext(basename)
    dst_file = os.path.join(output_folder, basename)

    xmp_src = os.path.join(os.path.dirname(filename), head + '.xmp')
    xmp_dst = os.path.join(output_folder, head + '.xmp')

    # rename if necessary
    count = 0
    while os.path.exists(dst_file):
        count += 1
        dst_file = os.path.join(output_folder, '%s-%d%s' % (head, count, tail))
        xmp_dst = os.path.join(output_folder, '%s-%d.xmp' % (head, count))

    # print 'Renaming %s to %s' % (file, dst_file)
    # print(filename, dst_file)
    # os.rename(file, dst_file)

    # copy the picture to the organised structure
    if not os.path.exists(dst_file) and not os.path.exists(xmp_dst):
        shutil.copy2(filename, dst_file)

        if (os.path.exists(xmp_src)):
            shutil.copy2(xmp_src, xmp_dst)

    # verify if file is the same and display output
    if hash_file(filename) == hash_file(dst_file) and hash_file(xmp_src) == hash_file(xmp_dst):
        # print('File copied with success to ' + dst_file)
        os.remove(filename)
        os.remove(xmp_src)
    else:
        print('File failed to copy :( ' + filename)

    return None


def get_exif_date(filename, tag):
    # Read file
    open_file = open(filename, 'rb')

    # Return Exif tags
    tags = exifread.process_file(open_file, details=False, stop_tag=tag)
    # tags = exifread.process_file(open_file, details=False)
    open_file.close()

    try:
        date_object = datetime.datetime.strptime(
            tags[tag].values, '%Y:%m:%d %H:%M:%S')

        # Date
        year = str(date_object.year)
        month = str(date_object.month).zfill(2)
        day = str(date_object.day).zfill(2)
        # Time
        hour = str(date_object.hour).zfill(2)
        minute = str(date_object.minute).zfill(2)
        second = str(date_object.second).zfill(2)

        # New Filename
        return {
            'date': year + month + day + '-' + hour + minute + second,
            'year': year,
            'month': month,
            'day': day,
        }

    except:
        return None


def convert_name_date(name_date):
    try:
        date_object = datetime.datetime.strptime(
            name_date, '%Y%m%d-%H%M%S')

        # Date
        year = str(date_object.year)
        month = str(date_object.month).zfill(2)
        day = str(date_object.day).zfill(2)
        # Time
        hour = str(date_object.hour).zfill(2)
        minute = str(date_object.minute).zfill(2)
        second = str(date_object.second).zfill(2)

        # New Filename
        return {
            'date': year + month + day + '-' + hour + minute + second,
            'year': year,
            'month': month,
            'day': day,
        }

    except:
        return None


def check_date_in_name(filename):
    basename = os.path.basename(filename)
    expression = r'^[12][019]\d{2}[01]\d[0-3]\d-[0-2]\d[0-5]\d[0-5]\d'
    match = re.match(expression, basename)
    if match:
        return convert_name_date(match.group())

    return None


limit = 5000
current = 0

# get all picture files from directory and process
for root, dirs, files in os.walk(source_path, topdown=False):
    if current >= limit:
        break

    for name in files:
        if current >= limit:
            break

        if name.endswith('.cr2') or name.endswith('.CR2'):
            current += 1
            filename = os.path.join(root, name)

            date_original = get_exif_date(filename, 'EXIF DateTimeOriginal')
            date_digitized = get_exif_date(filename, 'EXIF DateTimeDigitized')
            date_filename = check_date_in_name(filename)

            # All dates exists
            if date_original and date_digitized and date_filename:

                # Original != digitized
                if date_original['date'] != date_digitized['date']:

                    if date_filename['date'] == date_original['date']:
                        # move_file(filename, mismatch_digitized_path)
                        print('Digitized Mismatch -> Original/Filename {} | Digitized {}'.format(
                            date_original['date'], date_digitized['date']))
                        continue

                    if date_filename['date'] == date_digitized['date']:
                        print('Original Mismatch -> Original {} | Digitized/Filename {}'.format(
                            date_original['date'], date_digitized['date']))
                        # move_file(filename, mismatch_original_path)
                        continue

                    print('All Mismatch -> Original {} | Digitized {} | Filename {}'.format(
                        date_original['date'], date_digitized['date'], date_filename['date']))
                    # move_file(filename, mismatch_all_path)
                    continue

                if date_original['date'] != date_filename['date']:
                    print('Filename Mismatch -> Original/Digitized {} | Filename {}'.format(
                        date_original['date'], date_filename['date']))
                    # move_file(filename, mismatch_name_path, date_original)
                    continue

                # move_file(filename, match_path, date_original)
                continue

            # Two Values - No filename
            if date_original and date_digitized:
                if date_original['date'] == date_digitized['date']:
                    print(filename, 'no name match')
                    # move_file(filename, no_name_match_path)
                else:
                    print(filename, 'no name mismatch')
                    # move_file(filename, no_name_mismatch_path)
                continue

            # Two dates - no digitized
            if date_original and date_filename:
                if date_original['date'] == date_filename['date']:
                    print(filename, 'no digitized match')
                    # move_file(filename, no_digitized_match_path)
                else:
                    print(filename, 'no digitized mismatch')
                    # move_file(filename, no_digitized_mismatch_path)
                continue

            # Two dates - no original
            if date_filename and date_digitized:
                if date_digitized['date'] == date_filename['date']:
                    print(filename, 'no original match')
                    # move_file(filename, no_original_match_path)
                else:
                    print(filename, 'no original mismatch')
                    # move_file(filename, no_original_mismatch_path)
                continue

            # Only original
            if date_original:
                print(filename, 'only original')
                # move_file(filename, only_original_path, date_original)
                continue

            # Only digitized
            if date_digitized:
                print(filename, 'only digitized')
                # move_file(filename, only_digitized_path, date_digitized)
                continue

            # Only filename
            if date_filename:
                print(filename, 'only filename')
                # move_file(filename, only_filename_path, date_filename)
                continue

            # No dates
            print(filename, 'none')
            # move_file(filename, none_path)
            continue

print(current, 'files analyzed')
